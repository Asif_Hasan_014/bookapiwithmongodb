package com.letsstartcoding.springbootrestapiexample.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.letsstartcoding.springbootrestapiexample.model.Book;
import com.letsstartcoding.springbootrestapiexample.model.MyBook;
import com.letsstartcoding.springbootrestapiexample.repository.BookRepository;
import com.letsstartcoding.springbootrestapiexample.repository.MyBookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private MyBookRepository myBookRepository;

	public Book saveMongo(Book book) {
		bookRepository.save(book);
		return book;
	}
	
	public List<Book> getAll() {
		List<Book> bookList = bookRepository.findAll();
		return bookList;
	}
	
	public List<MyBook> insertMongoToMySql() {
		
		List<MyBook> myBookList = new ArrayList<MyBook>();
		
		List<Book> bookList = bookRepository.findAll();
		
		for(Book book : bookList) {
			MyBook myBook = new MyBook();
			
			myBook.setBookName(book.getBookName());
			myBook.setAuthorName(book.getAuthorName());
			myBook.setMongoId(book.getId());
			
			myBookRepository.save(myBook);
			
			myBookList.add(myBook);
		}
		
		return myBookList;
	}
}
