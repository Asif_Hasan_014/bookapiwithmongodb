package com.letsstartcoding.springbootrestapiexample.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.letsstartcoding.springbootrestapiexample.dao.BookService;
import com.letsstartcoding.springbootrestapiexample.model.Book;
import com.letsstartcoding.springbootrestapiexample.model.MyBook;

@RestController
@RequestMapping("/book")
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	@PostMapping("/addBookMongo")
	public Book createEmployee(@Valid @RequestBody Book book) {
		return bookService.saveMongo(book);
	}
	
	@GetMapping("/getAllBook")
	public List<Book> getAllBooks(){
		return bookService.getAll();
	}
	
	@GetMapping("/postFromMongoToMysql")
	public List<MyBook> insertRecordFromMongoToMySql(){
		return bookService.insertMongoToMySql();
	}
}
