package com.letsstartcoding.springbootrestapiexample.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.letsstartcoding.springbootrestapiexample.model.MyBook;


public interface MyBookRepository extends JpaRepository<MyBook, Long> {
	
}
