package com.letsstartcoding.springbootrestapiexample.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.letsstartcoding.springbootrestapiexample.model.Book;

public interface BookRepository extends MongoRepository<Book, Long>{

}
