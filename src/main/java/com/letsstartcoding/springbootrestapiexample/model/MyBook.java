package com.letsstartcoding.springbootrestapiexample.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Entity
@Data
public class MyBook {
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String bookName;
	private String authorName;
	private Long mongoId;
	
}
